/**
 * sindri-sql-pool.js
 *
 * Gerencia Conexão e Poll
 * Use esta classe quando desejar ter apenas uma conexão na aplicação
 *
 * Created by André Timermann on 29/10/2015
 *
 */
'use strict';

var mssql = require('mssql');
var config = require('config');
var logger = require('sindri-logger');

/**
 * Objeto de conexão,

 ready=>
  * Status da conexão
  * 0 -> Não iniciada
  * 1 -> Iniciando
  * 2 -> Pronta
  * 3 -> Erro
  */

var connection = {};


/**
 * Verifica se existe uma conexão disponível, se sim retorna
 * @return {Object Connection} Objeto de conexão
 */
function _getConnection(connectionName) {

    logger.debug("Obtendo conexão...");

    return Promise.resolve()

    .then(function() {

        ////////////////////////////////////////////////////////////////////////////
        /// SE CONEXÃO AINDA NÃO EXISTE
        ///////////////////////////////////////////////////////////////////////////////
        if (connection[connectionName] === undefined) {

            logger.debug("Conexão inexistente, criando...");

            // Nenhuma Conexão criada, inicia aqui
            createConnection(connectionName);

            return _getConnection(connectionName);

        }

        ////////////////////////////////////////////////////////////////////////////
        /// CONEXÃO FALHOU OU FOI FECHADA, REABRINDO
        ///////////////////////////////////////////////////////////////////////////////
        else if (connection[connectionName].ready === 0) {


            logger.debug("Reconectando...");

            // Nenhuma Conexão criada, inicia aqui
            createConnection(connectionName);

            return _getConnection(connectionName);

        }
        ////////////////////////////////////////////////////////////////////////////
        /// SE CONEXÃO AINDA ESTÁ INICIANDO
        ///////////////////////////////////////////////////////////////////////////////
        else if (connection[connectionName].ready === 1) {

            logger.debug("Iniciando conexão... aguardar 5 segundos... 2");


            return new Promise(function(resolve, reject) {
                logger.debug("Iniciando conexão... aguardar 5 segundos... 1");

                setTimeout(function() {

                    logger.debug("Iniciando conexão... aguardar 5 segundos... 0");

                    _getConnection(connectionName).then(resolve).catch(reject);


                    // TODO: Parametrizar timeout
                }, 5000);
            });


        }
        ////////////////////////////////////////////////////////////////////////////
        /// SE CONEXÃO ESTÁ PRONTA
        ///////////////////////////////////////////////////////////////////////////////
        else if (connection[connectionName].ready === 2) {

            logger.debug("Conexão Pronta retornando...");
            return connection[connectionName].connection;

        }
        ////////////////////////////////////////////////////////////////////////////
        /// ERRO, CONEXÃO NÃO DISPONÌVEL
        ///////////////////////////////////////////////////////////////////////////////
        else {

            // TODO: Verificar possibilidade de voltar status para 0 para nova tentativa, colocar no createConnect
            logger.error("Erro");
            throw new Error("Conexão não disponível!");

        }

    });


}

/**
 * Cria uma nova conexão
 * @return {[type]} [description]
 */
function createConnection(connectionName) {

    var connectionConfig = config.get("database." + connectionName);

    if (connectionConfig.engine !== 'odbc') {
        throw new Error("Engine deve ser odbc");
    }

    logger.debug('MSSQL: Criando nova conexão');
    logger.debug('HOST:', connectionConfig.host);
    logger.debug('USER:', connectionConfig.user);
    logger.debug('DATABASE:', connectionConfig.database);

    // Define configurações
    var mssqlConnectionConfig = {
        user: connectionConfig.user,
        password: connectionConfig.password,
        server: connectionConfig.host,
        database: connectionConfig.database,
        connectionTimeout: 30000,
        requestTimeout: 30000
    };

    // Port
    if (connectionConfig.port) {
        mssqlConnectionConfig.port = connectionConfig.port;
    }



    // Marca conexão como Iniciando
    connection[connectionName] = {
        ready: 1,
        connection: new mssql.Connection(mssqlConnectionConfig)
    };


    connection[connectionName].connection.connect(function(err) {
        if (err) {
            connection[connectionName].ready = 0;
            logger.error(err);

        } else {
            // Marca Conexão como ativada com sucesso
            logger.info('MSSQL: Conexão realizada com sucesso');
            connection[connectionName].ready = 2;
        }

    });

}

/**
 * Fecha Conexão
 *
 * @param  {[type]} connectionName [description]
 * @return {[type]}                [description]
 */
function closeConnection(connectionName) {

    logger.info("Fechando conexão " + connectionName + "...");

    ////////////////////////////////////////////////////////////////////////////
    /// SE CONEXÃO AINDA NÃO EXISTE
    ///////////////////////////////////////////////////////////////////////////////
    if (connection[connectionName] === undefined) {
        throw new Error("Conexão inexistente");

    }

    ////////////////////////////////////////////////////////////////////////////
    /// SE CONEXÃO AINDA ESTÁ INICIANDO
    ///////////////////////////////////////////////////////////////////////////////
    else if (connection[connectionName].ready == 1) {
        throw new Error("Conexão não inicializada");
    }
    ////////////////////////////////////////////////////////////////////////////
    /// SE CONEXÃO ESTÁ PRONTA
    ///////////////////////////////////////////////////////////////////////////////
    else if (connection[connectionName].ready == 2) {

        connection[connectionName].ready = 1;
        connection[connectionName].connection.close();
        logger.info("OK");
        connection[connectionName].ready = 0;

    }
    ////////////////////////////////////////////////////////////////////////////
    /// ERRO, CONEXÃO NÃO DISPONÌVEL
    ///////////////////////////////////////////////////////////////////////////////
    else {
        throw new Error("Conexão não disponível!");
    }


}

/**
 * Retorna Status da conexão
 *
 * 	NULL -> Não iniciado
 *  	0 -> Desconectada
 *    	1 -> Conectando...
 *     	2 -> Conectada
 *      3 -> Erro
 *
 * @param  {[type]} connectionName [description]
 * @return {[type]}                [description]
 */
function getConnectionStatus(connectionName) {

    return connection[connectionName] !== undefined ? connection[connectionName].ready : null;

}



////////////////////////////////////////////////////////////////////////////////////////////////v///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////v////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////v////////////////////////////////////////////////////////////

/**
 * Gerencia  POOL de conexões MSSQL
 */
class ConnMSSQL {
    constructor(connectionName) {

        this.connectionName = connectionName;

    }

    getConnectionStatus() {

        return getConnectionStatus(this.connectionName);

    }

    closeConnection() {

        return closeConnection(this.connectionName);

    }

    getConnection() {

        return _getConnection(this.connectionName);

    }

    /**
     * Retorna uma tranação, transação sempre será sequencial, cuidado com isso
     * @return {[type]} [description]
     */
    newTransaction() {

        return _getConnection(this.connectionName).then(function(conn) {

            logger.debug('Retornando Transação...');
            return new mssql.Transaction(conn);

        }).catch(function(err) {

            console.log('ERROR MARK 4 ----------------------------------------------------------------');
            console.log(err);
            return;

        });

    }


    /**
     * Executa uma Consulta MSSQL
     *
     * NOTA: PAra retornar ultimo ID use SELECT SCOPE_IDENTITY() AS ID na mesma query separado por ;
     *
     * @param  {string} sqlQuery Descrição da Consulta
     * @return {Promise}
     */
    query(sqlQuery, transaction) {

        var self = this;


        if (transaction) {

            var request = new mssql.Request(transaction); // or: var request = connection.request();
            request.verbose = logger.is('debug');
            return request.query(sqlQuery);


        } else {

            return _getConnection(this.connectionName).

            then(function(conn) {

                var request = conn.request(); // or: var request = connection.request();

                request.verbose = logger.is('debug');

                return request.query(sqlQuery);

            })

            .catch(function(err) {

                console.log('ERROR MARK 1----------------------------------------------------------------');
                console.log(err);
                throw err;

            });
        }

    }

}

module.exports = ConnMSSQL;
